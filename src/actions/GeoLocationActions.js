import _ from 'lodash';
import {
  SCOOTERS_INSIDE_RADIUS,
  MARKER_PIN_FETCH_SUCCESS,
} from './types';
import firebase from 'firebase';
import axios from 'axios';
import { Constants, Location, Permissions } from 'expo';

const TRACK_ONCE_GEOLOCATION_OPTIONS = { enableHighAccuracy: true, maximumAge: 10000 };
const CLOUD_FUNCTION_ROOT_URL = 'https://us-central1-bbeepscooter.cloudfunctions.net';

//Get user intial position for setting markerPin to user position
export const getPinLocation = () => {
  return async (dispatch) => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    const location = await Location.getCurrentPositionAsync(TRACK_ONCE_GEOLOCATION_OPTIONS);
    const { accuracy, altitude, latitude, longitude, speed, heading  } = location.coords;
    dispatch({ type: MARKER_PIN_FETCH_SUCCESS, payload: location.coords });
  }
}

//Send backend request to cloud functions fetch scooters in radius
export const getScootersInRadius = ({ latitude, longitude }) => {
  return (dispatch) => {
    axios.post(`${CLOUD_FUNCTION_ROOT_URL}/sendUserRadiusFilteredScooterLocations`,{'lat': latitude,'lng':longitude})
    .then((response) => filteredScooters(response.data, dispatch))
    .catch((error) => alert('Oops looks like there was error fetching scooters. Please try again.'));
  }
}

//Once scooters are fetched update them into the app state
const filteredScooters = ({ scootersInUserRadius }, dispatch) => {
  dispatch({ type: SCOOTERS_INSIDE_RADIUS, payload: scootersInUserRadius });
}
