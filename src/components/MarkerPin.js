/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions
} from 'react-native';

//WINDOW_MEASUREMENTS
const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const MarkerPin = () => {
  return (
    <View style={{ position: 'absolute', flex: 1, alignSelf: 'center', top: WINDOW_HEIGHT/2.4 }}>
      <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000', width: 20, height: 20, borderRadius: 20 }}>
        <View style={{ backgroundColor: '#FFFFFF', width: 10, height: 10, borderRadius: 10 }}/>
      </View>
      <View style={{ borderWidth: 0, borderColor: '#000000', width: 20, height: 20 }}>
        <View style={{ borderRightWidth: 3, borderColor: '#000000', width: 10, height: 20, zIndex: 2 }}/>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default MarkerPin;
