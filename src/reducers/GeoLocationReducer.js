import {
  SCOOTERS_INSIDE_RADIUS,
  MARKER_PIN_FETCH_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = { markerPin: null, noOfScootersInRadius: [] };

const geoLocationReducer = (state=INITIAL_STATE, actions) => {
  switch(actions.type) {
    case MARKER_PIN_FETCH_SUCCESS:
      return { ...state, markerPin: actions.payload }
    case SCOOTERS_INSIDE_RADIUS:
      return { ...state, noOfScootersInRadius: actions.payload }
    default:
      return { ...state };
  }
}

export default geoLocationReducer;
